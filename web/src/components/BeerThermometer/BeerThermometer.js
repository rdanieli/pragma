import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './BeerThermometer.css'

class BeerThermometer extends Component {
  constructor() {
    super();

    this.state = {
      previousTimestamp: -1,
      chartDataset: []
    }
  }

  componentDidMount() {
    this.setState({ previousTimestamp: this.props.temperatureInfo.timestamp });
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.temperatureInfo.timestamp !== this.state.previousTimestamp) {
      this.setState({ 
        previousTimestamp: nextProps.temperatureInfo.timestamp
      });
    }
  }

  render() {
    const beerInfo = this.props.temperatureInfo.beerInfo;
    const maxTemp = beerInfo.maxTemp;
    const minTemp = beerInfo.minTemp;
    const statusBool = this.props.temperatureInfo.temperature <= maxTemp && this.props.temperatureInfo.temperature >= minTemp;

    return (
      <div className="BeerThermometer">
        <div className="text-container">
          <label>Temperature {this.props.temperatureInfo.temperature.toFixed(2)}º C</label>
          <label>Type: {this.props.temperatureInfo.beerInfo.name}</label>
          <label>Status: {!statusBool && "Not"} OK</label>
        </div> 
      </div>
    );
  }
}

BeerThermometer.propTypes = {
  temperatureInfo: PropTypes.object.isRequired
}

export default BeerThermometer;
